// INCLUDES - standard lib
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

// INCLUDES - FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// INCLUDES - Project specific
#include "utils.h"
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"
#include "accelerometer_driver.h"
#include "uart_driver.h"
#include "grlib.h"
#include "Crystalfontz128x128_ST7735.h"

/*********************************************************************************************/
/** DEFINES                                                                                  */
/*********************************************************************************************/

#define TIMER_PERIOD                            32768
#define LCD_LENGTH                              20

#define TRUE                                    1
#define FALSE                                   0

#define LCD_MSG(STR_DST, STR_LABEL, STR_VALUE)                      \
{                                                                   \
    my_ftoa(STR_VALUE, g_str_value, 2);                             \
    strcpy(STR_DST, STR_LABEL);                                     \
    strcat(STR_DST, g_str_value);                                   \
    t_displayWriter_dirty = TRUE;                                   \
}                                                                   \

#define xMutexTake                              xSemaphoreTake
#define xMutexGive                              xSemaphoreGive

#define TEMPERATURE_INITIAL                     20.0f

#define TASK_HEARTBEAT_PRIO                     (tskIDLE_PRIORITY + 1)
#define TASK_HEARTBEAT_TIMEOUT_ON               10
#define TASK_HEARTBEAT_TIMEOUT_OFF              990

#define TASK_SENSORS_READER_PRIO                (tskIDLE_PRIORITY + 2)
#define TASK_SENSORS_READER_TIMEOUT             100
#define TASK_SENSOR_TYPE_TEMPERATURE            0
#define TASK_SENSOR_TYPE_LIGHT                  1
#define TASK_SENSORS_READER_TEMPERATURE_BUFFER  10
#define TASK_SENSORS_LIGHT_LOOP_0               5
#define TASK_SENSORS_LIGHT_LOOP_1               10
#define TASK_SENSORS_TEMPERATURE_LOOP           10
#define TASK_SENSOR_BOARD_TEMPERATURE_LOOP      (60)
#define TASK_SENSOR_QUEUE_LENGTH                (10)

#define TASK_ACC_READER_PRIO                    (tskIDLE_PRIORITY + 2)
#define TASK_ACC_READER_TIMEOUT                 100
#define TASK_ACC_TEMPERATURE_LOOP               (30 * 10)
#define TASK_ACC_TEMPERATURE_V0                 1.567f
#define TASK_ACC_TEMPERATURE_T0                 -40.0f
#define TASK_ACC_TEMPERATURE_V1                 1.732f
#define TASK_ACC_TEMPERATURE_T1                 85.0f
#define TASK_ACC_QUEUE_LENGTH                   (20)

#define TASK_LCD_WRITER_PRIO                    (tskIDLE_PRIORITY + 2)
#define TASK_LCD_WRITER_TIMEOUT                 10
#define TASK_LCD_WRITER_TEMPERATURE_LOOP        60
#define TASK_LCD_WRITER_LIGHT_LOOP              2
#define TASK_LCD_WRITER_ACC_LOOP                10

// Timer_A UpMode Configuration
const Timer_A_UpModeConfig upConfig =
{
        TIMER_A_CLOCKSOURCE_ACLK,               // ACLK source = 32.768 kHz
        TIMER_A_CLOCKSOURCE_DIVIDER_1,          // ACLK/1
        TIMER_PERIOD,                           // 0.5 seconds
        TIMER_A_TAIE_INTERRUPT_DISABLE,         // Disable Timer interrupt
        TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE,     // Enable CCR0 interrupt
        TIMER_A_DO_CLEAR                        // Clear value
};

typedef float acc_rawdata_t[3];                 //accelerometer RAW data structure

typedef struct {                                //accelerometer  data structure
    float x;
    float y;
    float z;
} acc_data_t;

typedef struct {                                //sensor data structure
    uint8_t type;
    float value;
} sensor_data_t;

/*********************************************************************************************/
/** FUNCTIONS DECLARATION                                                                    */
/*********************************************************************************************/

static void setupHardware (void);
static void setupSensors (void);
static void setupAccelerometer (void);
static void setupDisplay (void);
static void setupUART(void);

static void task_heartBeat (void *pvParameters);
static void task_sensorsReader (void *pvParameters);
static void task_accelerometerReader (void *pvParameters);
static void task_displayWriter (void *pvParameters);

void TA1_0_IRQHandlerCallback (void);
void PORT5_IRQHandler (void);
void EUSCIA0_IRQHandlerCallback();

/*********************************************************************************************/
/** GLOBAL VARIABLES DECLARATION                                                             */
/*********************************************************************************************/

float g_temperature;                                    /* board temperature (avg) */

static SemaphoreHandle_t g_mutexI2C;                    /* mutex for I2C bus */
static SemaphoreHandle_t g_MutexTemperature;            /* mutex for variable 'g_temperature' */

static SemaphoreHandle_t g_semaphoreDisplay;            /* semaphore - display other task coordination */

static QueueHandle_t g_sensorData;                      /* queue temperature+light sensor readings */
static QueueHandle_t g_accelerometerData;               /* queue accelerometer readings */

static Graphics_Context g_sContext;                     /* LCD context */

static char g_str_header0[] = "SE PRACT 2017";
static char g_str_header1[] = "-------------";

static char g_str_accel_x[LCD_LENGTH];
static char g_str_accel_y[LCD_LENGTH];
static char g_str_accel_z[LCD_LENGTH];

static char g_str_temperature[LCD_LENGTH];
static char g_str_light[LCD_LENGTH];

static char g_str_value[LCD_LENGTH];

static char g_str_author[] = "Miquel Angel Alcalde";

volatile SemaphoreHandle_t xBinarySemaphore;            /* Declaracion de un semaforo binario de ADC driver */

/*********************************************************************************************/
/** TASK VARIABLES DECLARATION                                                             */
/*********************************************************************************************/
static TickType_t t_heartBeat_timeout_ON;              /* delay ON status for 'task_heartBeat' task*/
static TickType_t t_heartBeat_timeout_OFF;             /* delay OFF status for 'task_heartBeat' task*/

static uint16_t  t_sensorsReader_loop;                   /* loop counter for 'task_sensorsReader' task*/
static uint16_t  t_sensorsReader_temperature_loop;       /* update temperature loop counter for 'task_sensorsReader' task*/
static float  t_sensorsReader_light;                    /* current light for 'task_sensorsReader' task*/
static float  t_sensorsReader_temperature;              /* current temperature for 'task_sensorsReader' task*/
static uint16_t t_sensorsReader_light_raw;              /* light raw value from sensor */
static sensor_data_t t_sensorsReader_temperature_data;  /* temperature queue item */
static sensor_data_t t_sensorsReader_light_data;        /* temperature queue item */
static TickType_t t_sensorReader_timeout;               /* wake up interval for 'task_sensorsReader' task*/
static TickType_t t_sensorReader_wakeUpTime;            /* last wake up  for 'task_sensorsReader' task*/
static uint8_t  t_sensorsReader_dirty;                  /* flag to control if task has new data to be published*/

static uint16_t  t_accReader_loop;                       /* loop counter for 'task_accelerometerReader' task*/
static float  t_accReader_temperature;                  /* board temperature for 'task_accelerometerReader' task*/
static float  t_accReader_temperature_offsetXY;         /* correction offset XY for temperature for 'task_accelerometerReader' task*/
static float  t_accReader_temperature_offsetZ;          /* correction offset XY for 'task_accelerometerReader' task*/
static acc_data_t t_accReader_data;                     /* current accelerometer values */
static acc_rawdata_t accReader_data_raw;                /* raw accelerometer values from sensor */
static TickType_t t_accReader_timeout;                  /* wake up interval for 'task_accelerometerReader' task*/
static TickType_t t_accReader_wakeUpTime;               /* last wake up  for task_accelerometerReader'' task*/
static uint8_t  t_accReader_dirty;                      /* flag to control if task has new data to be published*/

static uint16_t  t_displayWriter_loop_temperature;       /* current light for 'task_displayWriter' task*/
static uint16_t  t_displayWriter_loop_light;             /* current light for 'task_displayWriter' task*/
static uint16_t  t_displayWriter_loop_acc;               /* current acc for 'task_displayWriter' task*/
static sensor_data_t t_displayWriter_sensor_data;       /* sensor light/temperature queue item */
static float  t_displayWriter_light;                    /* current temperature for 'task_displayWriter' task*/
static float  t_displayWriter_temperature;              /* current temperature for 'task_displayWriter' task*/
static float  t_displayWriter_temperature_previous;     /* previous temperature for 'task_displayWriter' task*/
static acc_data_t t_displayWriter_acc;                  /* current accelerometer values  for 'task_displayWriter' task*/
static acc_data_t t_displayWriter_acc_avg;              /* current accelerometer values  for 'task_displayWriter' task*/
static uint8_t  t_displayWriter_has_previous_temperature; /* flag to control if task has previous temperature value*/
static uint8_t  t_displayWriter_dirty;                  /* flag to control if task has new data to be published*/

/*********************************************************************************************/
/** FUNCTIONS DEFINITION                                                                     */
/*********************************************************************************************/

int main(void)
{
    xBinarySemaphore = xSemaphoreCreateBinary();
    configASSERT(NULL != xBinarySemaphore);

    g_semaphoreDisplay = xSemaphoreCreateBinary();
    configASSERT(NULL != g_semaphoreDisplay);

    g_mutexI2C = xSemaphoreCreateMutex();
    configASSERT(NULL != g_mutexI2C);

    g_MutexTemperature = xSemaphoreCreateMutex();
    configASSERT(NULL != g_MutexTemperature);

    g_sensorData = xQueueCreate(TASK_SENSOR_QUEUE_LENGTH, sizeof(sensor_data_t));
    configASSERT(NULL != g_sensorData);

    g_accelerometerData = xQueueCreate(TASK_ACC_QUEUE_LENGTH, sizeof(acc_data_t));
    configASSERT(NULL != g_accelerometerData);

    g_temperature = TEMPERATURE_INITIAL;

    memset(g_str_light, 0, LCD_LENGTH);
    memset(g_str_temperature,0 , LCD_LENGTH);
    memset(g_str_accel_x, 0, LCD_LENGTH);
    memset(g_str_accel_y, 0, LCD_LENGTH);
    memset(g_str_accel_z, 0, LCD_LENGTH);

    setupHardware();
    setupSensors();
    setupAccelerometer();
    setupDisplay();

    MAP_Interrupt_enableMaster();

    xTaskCreate(task_heartBeat,
                "Heart-beat task",
                configMINIMAL_STACK_SIZE,
                NULL,
                TASK_HEARTBEAT_PRIO,
                NULL);
    xTaskCreate(task_sensorsReader,
                "Temperature/Light reader task",
                configMINIMAL_STACK_SIZE,
                NULL,
                TASK_SENSORS_READER_PRIO,
                NULL);
    xTaskCreate(task_accelerometerReader,
                "Accelerometer reader task",
                configMINIMAL_STACK_SIZE,
                NULL,
                TASK_ACC_READER_PRIO,
                NULL);
    xTaskCreate(task_displayWriter,
                "LCD writer task",
                4 * configMINIMAL_STACK_SIZE,
                NULL,
                TASK_LCD_WRITER_PRIO,
                NULL);

    vTaskStartScheduler();


    return 0;
}
/*--------------------------------------------------------------------------------------------*/
static void setupHardware(void)
{

    extern void FPU_enableModule(void);

    /* Halting WDT and disabling master interrupts */
    //MAP_WDT_A_holdTimer();
    //MAP_Interrupt_disableMaster();

    /* Set the core voltage level to VCORE1 */
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE1);

    /* Set 2 flash wait states for Flash bank 0 and 1*/
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    /* Initializes Clock System */
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_48);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1 );
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1 );
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1 );
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    // Habilita la FPU
    MAP_FPU_enableModule();

    // Inicializacion de pins sobrantes para reducir consumo
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Configuracion del pin P1.0 (LED rojo) como salida
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
}
/*--------------------------------------------------------------------------------------------*/
static void setupSensors (void)
{
    I2C_init();

    TMP006_init();

    sensorOpt3001Init();
    sensorOpt3001Enable(true);
}
/*--------------------------------------------------------------------------------------------*/
static void setupAccelerometer (void)
{
    Accel_init();
}
/*--------------------------------------------------------------------------------------------*/
static void setupDisplay (void)
{
    /* Initializes display */
    Crystalfontz128x128_Init();

    /* Set default screen orientation */
    Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_UP);

    /* Initializes graphics context */
    Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    GrContextFontSet(&g_sContext, &g_sFontFixed6x8);

    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext,
                                g_str_header0,
                                AUTO_STRING_LENGTH,
                                65,
                                22,
                                OPAQUE_TEXT);
    Graphics_drawStringCentered(&g_sContext,
                                g_str_header1,
                                AUTO_STRING_LENGTH,
                                65,
                                30,
                                OPAQUE_TEXT);

    Graphics_drawString(&g_sContext,
                        g_str_author,
                        GRAPHICS_AUTO_STRING_LENGTH,
                        1,
                        121,
                        GRAPHICS_TRANSPARENT_TEXT);

}
/*--------------------------------------------------------------------------------------------*/
static void setupUART(void)
{
    UartInit(EUSCIA0_IRQHandlerCallback);
}
/*--------------------------------------------------------------------------------------------*/
static void task_heartBeat (void *pvParameters)
{
    t_heartBeat_timeout_ON = pdMS_TO_TICKS(TASK_HEARTBEAT_TIMEOUT_ON);
    t_heartBeat_timeout_OFF = pdMS_TO_TICKS(TASK_HEARTBEAT_TIMEOUT_OFF);

    for(;;) {
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(t_heartBeat_timeout_ON);
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay(t_heartBeat_timeout_OFF);
    }
}
/*--------------------------------------------------------------------------------------------*/
static void task_sensorsReader (void *pvParameters)
{
    t_sensorReader_timeout = pdMS_TO_TICKS(TASK_SENSORS_READER_TIMEOUT);

    t_sensorsReader_loop = 1;
    t_sensorsReader_temperature_loop = 1;
    t_sensorsReader_light = 0.0f;
    t_sensorsReader_temperature = 0.0f;

    t_sensorsReader_dirty = FALSE;

    t_sensorsReader_light_raw = 0;

    t_sensorsReader_light_data.type = TASK_SENSOR_TYPE_LIGHT;
    t_sensorsReader_light_data.value = 0.0f;

    t_sensorsReader_temperature_data.type = TASK_SENSOR_TYPE_TEMPERATURE;
    t_sensorsReader_temperature_data.value = 0.0f;

    for(;;) {
        t_sensorReader_wakeUpTime = xTaskGetTickCount();


        //step 1 - light reading from sensor
        {
            if ((TASK_SENSORS_LIGHT_LOOP_0 == t_sensorsReader_loop) ||
                (TASK_SENSORS_LIGHT_LOOP_1 == t_sensorsReader_loop))
            {
//              if (pdPASS == xMutexTake(g_mutexI2C, 0))
                {
                    sensorOpt3001Read(&t_sensorsReader_light_raw);
                    sensorOpt3001Convert(t_sensorsReader_light_raw, &t_sensorsReader_light);

                    t_sensorsReader_light_data.value = t_sensorsReader_light;
                    xQueueSend(g_sensorData, &t_sensorsReader_light_data, 0);

                    t_sensorsReader_dirty = TRUE;

//                  xMutexGive(g_mutexI2C);
                }
            }
        }

        //step 2 - temperature reading from sensor
        {
//          if (pdPASS == xMutexTake(g_mutexI2C, 0))
            {
                t_sensorsReader_temperature = TMP006_readAmbientTemperature();
                t_sensorsReader_temperature_data.value += t_sensorsReader_temperature;

                if (TASK_SENSORS_TEMPERATURE_LOOP == t_sensorsReader_loop)
                {
                    t_sensorsReader_temperature_data.value /= TASK_SENSORS_TEMPERATURE_LOOP;
                    xQueueSend(g_sensorData, &t_sensorsReader_temperature_data, 0);

                    t_sensorsReader_temperature_data.value = 0.0f;
                    t_sensorsReader_loop = 0;
                    t_sensorsReader_temperature_loop += 1;

                    t_sensorsReader_dirty = TRUE;
                }

//              xMutexGive(g_mutexI2C);
            }
        }

        //step 3 - updating global temperature variable
        if (TASK_SENSOR_BOARD_TEMPERATURE_LOOP == t_sensorsReader_temperature_loop)
        {
            if (pdPASS == xMutexTake(g_MutexTemperature, 0))
            {
                g_temperature = t_sensorsReader_temperature;
                xMutexGive(g_MutexTemperature);
            }

            t_sensorsReader_temperature_loop = 1;
        }

        t_sensorsReader_loop += 1;

        if (TRUE == t_sensorsReader_dirty)
        {
            xSemaphoreGive(g_semaphoreDisplay);
            t_sensorsReader_dirty = FALSE;
        }

        vTaskDelayUntil(&t_sensorReader_wakeUpTime, t_sensorReader_timeout);
    }
}
/*--------------------------------------------------------------------------------------------*/
static void task_accelerometerReader (void *pvParameters)
{
    t_accReader_timeout = pdMS_TO_TICKS(TASK_ACC_READER_TIMEOUT);

    t_accReader_loop = 1;

    t_accReader_temperature = TEMPERATURE_INITIAL;

    t_accReader_temperature_offsetXY = (0.7 *0.001 * t_accReader_temperature);
    t_accReader_temperature_offsetZ = (0.4 *0.001 * t_accReader_temperature);

    t_accReader_data.x = 0.0f;
    t_accReader_data.y = 0.0f;
    t_accReader_data.z = 0.0f;

    accReader_data_raw[0] = 0.0f;
    accReader_data_raw[1] = 0.0f;
    accReader_data_raw[2] = 0.0f;

    for(;;)
    {
        t_accReader_wakeUpTime = xTaskGetTickCount();

        Accel_read(accReader_data_raw);

        t_accReader_data.x = accReader_data_raw[0] + t_accReader_temperature_offsetXY;
        t_accReader_data.y = accReader_data_raw[1] + t_accReader_temperature_offsetXY;
        t_accReader_data.z = accReader_data_raw[2] + t_accReader_temperature_offsetZ;

        xQueueSend(g_accelerometerData, &t_accReader_data, 0);

        //step 2 - refreshing global temperature variable
        if (TASK_ACC_TEMPERATURE_LOOP == t_accReader_loop)
        {
            if (pdPASS == xMutexTake(g_MutexTemperature, 0))
            {
                t_accReader_temperature = g_temperature;

                t_accReader_temperature_offsetXY = (0.7 * 0.001 * t_accReader_temperature);
                t_accReader_temperature_offsetZ = (0.4 *0.001 * t_accReader_temperature);

                xMutexGive(g_MutexTemperature);
            }

            t_accReader_loop = 0;
        }

        t_accReader_loop += 1;

        xSemaphoreGive(g_semaphoreDisplay);

        vTaskDelayUntil(&t_accReader_wakeUpTime, t_accReader_timeout);
    }
}
/*--------------------------------------------------------------------------------------------*/
static void task_displayWriter (void *pvParameters)
{
    t_displayWriter_loop_light = 0;
    t_displayWriter_loop_temperature = 0;

    t_displayWriter_loop_acc =  0;

    t_displayWriter_light = 0.0f;

    t_displayWriter_temperature = 0.0f;
    t_displayWriter_temperature_previous = 0.0f;

    t_displayWriter_acc.x = 0.0f;
    t_displayWriter_acc.y = 0.0f;
    t_displayWriter_acc.z = 0.0f;

    t_displayWriter_acc_avg.x = 0.0f;
    t_displayWriter_acc_avg.y = 0.0f;
    t_displayWriter_acc_avg.z = 0.0f;

    t_displayWriter_has_previous_temperature = FALSE;
    t_displayWriter_dirty = FALSE;

    for(;;)
    {
        if (xSemaphoreTake(g_semaphoreDisplay, portMAX_DELAY) == pdPASS);

        //step 1 - check sensor queue
        if (pdPASS == xQueueReceive(g_sensorData, &t_displayWriter_sensor_data, 0))
        {
            if (TASK_SENSOR_TYPE_TEMPERATURE == t_displayWriter_sensor_data.type) {

                t_displayWriter_loop_temperature += 1;
                t_displayWriter_temperature += t_displayWriter_sensor_data.value;

                if (TASK_LCD_WRITER_TEMPERATURE_LOOP == t_displayWriter_loop_temperature)
                {
                    t_displayWriter_temperature /= TASK_LCD_WRITER_TEMPERATURE_LOOP;

                    LCD_MSG(g_str_temperature, "temp: ", t_displayWriter_temperature);

                    if (TRUE == t_displayWriter_has_previous_temperature)
                    {
                        t_displayWriter_temperature_previous = (t_displayWriter_temperature - t_displayWriter_temperature_previous);

                        my_ftoa(t_displayWriter_temperature_previous, g_str_value, 2);
                        strcat(g_str_temperature, ", ");
                        strcat(g_str_temperature, g_str_value);
                    }

                    t_displayWriter_has_previous_temperature = TRUE;
                    t_displayWriter_temperature_previous = t_displayWriter_temperature;
                    t_displayWriter_loop_temperature = 0;
                }

            } else if (TASK_SENSOR_TYPE_LIGHT == t_displayWriter_sensor_data.type) {

                t_displayWriter_loop_light += 1;
                t_displayWriter_light += t_displayWriter_sensor_data.value;

                if (TASK_LCD_WRITER_LIGHT_LOOP == t_displayWriter_loop_light)
                {
                    t_displayWriter_light = t_displayWriter_light / TASK_LCD_WRITER_LIGHT_LOOP;

                    LCD_MSG(g_str_light, "light: ", t_displayWriter_light);

                    t_displayWriter_light = 0.0f;
                    t_displayWriter_loop_light = 0;
                }
            }

            if (pdPASS == xQueueReceive(g_accelerometerData, &t_displayWriter_acc, 0))
            {
                t_displayWriter_loop_acc += 1;

                t_displayWriter_acc_avg.x += t_displayWriter_acc.x;
                t_displayWriter_acc_avg.y += t_displayWriter_acc.y;
                t_displayWriter_acc_avg.z += t_displayWriter_acc.z;

                if (TASK_LCD_WRITER_ACC_LOOP == t_displayWriter_loop_acc)
                {
                    t_displayWriter_acc_avg.x /= TASK_LCD_WRITER_ACC_LOOP;
                    t_displayWriter_acc_avg.y /= TASK_LCD_WRITER_ACC_LOOP;
                    t_displayWriter_acc_avg.z /= TASK_LCD_WRITER_ACC_LOOP;

                    LCD_MSG(g_str_accel_x, "acc X: ", t_displayWriter_acc_avg.x);
                    LCD_MSG(g_str_accel_y, "acc Y: ", t_displayWriter_acc_avg.y);
                    LCD_MSG(g_str_accel_z, "acc Z: ", t_displayWriter_acc_avg.z);

                    t_displayWriter_acc_avg.x = 0.0f;
                    t_displayWriter_acc_avg.y = 0.0f;
                    t_displayWriter_acc_avg.z = 0.0f;

                    t_displayWriter_loop_acc = 0;
                }
            }
        }


        if (TRUE == t_displayWriter_dirty)
        {
            Graphics_clearDisplay(&g_sContext);

            Graphics_drawStringCentered(&g_sContext,
                                        g_str_header0,
                                        AUTO_STRING_LENGTH,
                                        65,
                                        22,
                                        OPAQUE_TEXT);

            Graphics_drawStringCentered(&g_sContext,
                                        g_str_header1,
                                        AUTO_STRING_LENGTH,
                                        65,
                                        30,
                                        OPAQUE_TEXT);

            Graphics_drawString(&g_sContext,
                                g_str_author,
                                GRAPHICS_AUTO_STRING_LENGTH,
                                1,
                                121,
                                GRAPHICS_TRANSPARENT_TEXT);



            Graphics_drawString(&g_sContext,
                                g_str_temperature,
                                GRAPHICS_AUTO_STRING_LENGTH,
                                10,
                                61,
                                GRAPHICS_TRANSPARENT_TEXT);

            Graphics_drawString(&g_sContext,
                                g_str_light,
                                GRAPHICS_AUTO_STRING_LENGTH,
                                10,
                                51,
                                GRAPHICS_TRANSPARENT_TEXT);

            Graphics_drawString(&g_sContext,
                                g_str_accel_x,
                                GRAPHICS_AUTO_STRING_LENGTH,
                                10,
                                81,
                                GRAPHICS_TRANSPARENT_TEXT);
            Graphics_drawString(&g_sContext,
                                g_str_accel_y,
                                GRAPHICS_AUTO_STRING_LENGTH,
                                10,
                                91,
                                GRAPHICS_TRANSPARENT_TEXT);
            Graphics_drawString(&g_sContext,
                                g_str_accel_z,
                                GRAPHICS_AUTO_STRING_LENGTH,
                                10,
                                101,
                                GRAPHICS_TRANSPARENT_TEXT);

            Graphics_flushBuffer(&g_sContext);

            t_displayWriter_dirty = FALSE;
        }
        taskYIELD();
    }
}
/*--------------------------------------------------------------------------------------------*/
void TA1_0_IRQHandler (void)
{
    static BaseType_t xHigherPriorityTaskWoken;

    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A1_BASE,TIMER_A_CAPTURECOMPARE_REGISTER_0);

    xHigherPriorityTaskWoken = pdFALSE;
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken );
}
/*--------------------------------------------------------------------------------------------*/
void PORT5_IRQHandler (void)
{
    static BaseType_t xHigherPriorityTaskWoken;

    uint32_t status;
    status = MAP_GPIO_getEnabledInterruptStatus(GPIO_PORT_P5);
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P5, status);

    if (status & GPIO_PIN1)
    {
        xHigherPriorityTaskWoken = pdFALSE;
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}
/*--------------------------------------------------------------------------------------------*/
void EUSCIA0_IRQHandlerCallback (void)
{
}

