/*
 * utils.h
 *
 *  Created on: Dec 9, 2017
 *      Author: codemonkey
 */


/* Reverses a string 'str' of length 'len' */
void my_reverse(char *str, int len);

/* Converts a given integer x to string str[].  d is the number
  of digits required in output. If d is more than the number
  of digits in x, then 0s are added at the beginning */
int my_intToStr(int x, char str[], int d);


/* Converts a floating point number to string. */
void my_ftoa(float n, char *res, int afterpoint);

